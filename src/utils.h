/*
 * This file is part of the trss.
 * Copyright (c) 2021 Stefan Ascher
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <string>
#include <vector>

std::string expand_path(const char* path);
void run_command(const std::string& command);

inline bool is_white(const std::string& s)
{
    std::string::const_iterator it = s.begin();
    while (it != s.end() && std::isspace(*it)) ++it;
    return !s.empty() && it == s.end();
}
inline std::vector<std::string> split(const std::string& str, const std::string& delim, bool keepEmpty = false, bool keepWhite = true)
{
    std::vector<std::string> result;

    size_t beg = 0;
    for (size_t end = 0; (end = str.find(delim, end)) != std::string::npos; ++end)
    {
        auto substring = str.substr(beg, end - beg);
        beg = end + delim.length();
        if (!keepEmpty && substring.empty())
            continue;
        if (!keepWhite && is_white(substring))
            continue;
        result.push_back(substring);
    }

    auto substring = str.substr(beg);
    if (substring.empty())
    {
        if (keepEmpty)
            result.push_back(substring);
    }
    else if (is_white(substring))
    {
        if (keepWhite)
            result.push_back(substring);
    }
    else
        result.push_back(substring);

    return result;
}
