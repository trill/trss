/*
 * This file is part of the trss.
 * Copyright (c) 2021 Stefan Ascher
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "url.h"

#include <algorithm>
#include <cctype>
#include <functional>
#include <string>

url::url(const std::string& aurl)
{
    parse(aurl);
}

void url::parse(const std::string& aurl)
{
    const std::string prot_end("://");
    std::string::const_iterator prot_i = std::search(aurl.begin(), aurl.end(), prot_end.begin(), prot_end.end());
    scheme_.reserve(std::distance(aurl.begin(), prot_i));
    std::transform(
        aurl.begin(), prot_i, std::back_inserter(scheme_), tolower); // protocol is icase
    if (prot_i == aurl.end())
        return;
    advance(prot_i, prot_end.length());
    std::string::const_iterator path_i = find(prot_i, aurl.end(), '/');
    host_.reserve(std::distance(prot_i, path_i));
    std::transform(prot_i, path_i, back_inserter(host_), tolower); // host is icase
    std::string::const_iterator query_i = find(path_i, aurl.end(), '?');
    path_.assign(path_i, query_i);
    if (query_i != aurl.end())
        ++query_i;
    query_.assign(query_i, aurl.end());
}

std::string url::scheme_host() const
{
    return scheme_ + "://" + host_;
}

std::string url::path_query() const
{
    return path_ + "?" + query_;
}
