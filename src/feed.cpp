/*
 * This file is part of the trss.
 * Copyright (c) 2021-2022 Stefan Ascher
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "feed.h"
#define CPPHTTPLIB_OPENSSL_SUPPORT
#include <httplib.h>
#include "url.h"
#include <iostream>
#include "date.h"

feed::feed(std::string url) :
    url_(std::move(url))
{
}

void feed::parse_feed(pugi::xml_node node, bool all)
{
    // https://www.youtube.com/feeds/videos.xml?channel_id=UC3ts8coMP645hZw9JSD3pqQ
    /*
<feed>
    <link rel="self" href="http://www.youtube.com/feeds/videos.xml?channel_id=UC3ts8coMP645hZw9JSD3pqQ"/>
    <id>yt:channel:UC3ts8coMP645hZw9JSD3pqQ</id>
    <yt:channelId>UC3ts8coMP645hZw9JSD3pqQ</yt:channelId>
    <title>Andreas Kling</title>
    <link rel="alternate" href="https://www.youtube.com/channel/UC3ts8coMP645hZw9JSD3pqQ"/>
    <author>
    </author>
    <published>2012-11-16T18:04:06+00:00</published>
    <!-- Entries start here -->
    <entry>
        <id>yt:video:b3a5V45LLss</id>
        <yt:videoId>b3a5V45LLss</yt:videoId>
        <yt:channelId>UC3ts8coMP645hZw9JSD3pqQ</yt:channelId>
        <title>
            Browser hacking: Let's get the "Canvas Cycle" JS demo running in our Browser!
        </title>
        <link rel="alternate" href="https://www.youtube.com/watch?v=b3a5V45LLss"/>
        <author>...</author>
        <published>2021-03-16T03:58:11+00:00</published>
        <updated>2021-03-16T03:58:11+00:00</updated>
        <media:group>
            <media:title>
                Browser hacking: Let's get the "Canvas Cycle" JS demo running in our Browser!
            </media:title>
            <media:description>
                Canvas Cycle demo: http://www.effectgames.com/demos/canvascycle/ SerenityOS is open source on GitHub: https://github.com/SerenityOS/serenity Merch: https://teespring.com/stores/serenityos Follow me on Twitter: https://twitter.com/awesomekling Sponsor me on GitHub: https://github.com/sponsors/awesomekling Support me on Patreon: https://patreon.com/serenityos Donate via Paypal: https://paypal.me/awesomekling SerenityOS is a Unix-like operating system that I'm implementing from scratch. http://serenityos.org
            </media:description>
        </media:group>
    </entry>
     */
    auto feedTitleNode = node.child("title");
    if (feedTitleNode && feedTitleNode.text())
        title_ = feedTitleNode.text().as_string();

    std::time_t last = entries_.empty() ? 0 : entries_.front().published;

    for (const auto& entryIt : node.children("entry"))
    {
        entry e;
        e.read = false;

        auto linkNode = entryIt.child("link");
        if (!linkNode)
            continue;
        auto linkAttr = linkNode.attribute("href");
        if (!linkAttr)
            continue;
        e.url = linkAttr.as_string();

        auto titleNode = entryIt.child("title");
        if (titleNode && titleNode.text())
            e.title = titleNode.text().as_string();
        auto publishedNode = entryIt.child("published");
        if (publishedNode && publishedNode.text())
        {
            std::istringstream in{publishedNode.text().as_string()};
            date::sys_seconds tp;
            in >> date::parse("%Y-%m-%dT%H:%M:%S", tp);
            e.published = tp.time_since_epoch().count();
        }
        else
            continue;
        if (last < e.published || all)
        {
            entries_.push_back(std::move(e));
        }
    }
    std::sort(entries_.begin(), entries_.end(), [](const auto& a, const auto& b)
    {
        return a.published > b.published;
    });
}

void feed::parse_rss(pugi::xml_node node, bool all)
{
    /*
<rss version="2.0">
  <channel>
    <title>0x2a.wtf</title>
    <link>https://ba.home.lan/0x2a/</link>
    <pubDate>Wed, 17 Mar 2021 08:53:20 +0100</pubDate>
    <description>RSS feed for 0x2a.wtf</description>
    <item>
        <title>AB Admin</title>
        <description><![CDATA[...]]></description>
        <link>https://ba.home.lan/0x2a/projects/abx/Cluster_Admin</link>
        <pubDate>Fri, 02 Nov 2018 14:29:13 +0100</pubDate>
    </item>
    <item>
        <title>ABx</title>
        <description><![CDATA[...]]></description>
        <link>https://ba.home.lan/0x2a/projects/abx/</link>
        <pubDate>Wed, 29 Aug 2018 00:00:00 +0200</pubDate>
    </item>
     */

    auto channel = node.child("channel");
    if (!channel)
        return;
    auto feedTitleNode = channel.child("title");
    if (feedTitleNode && feedTitleNode.text())
        title_ = feedTitleNode.text().as_string();

    std::time_t last = entries_.empty() ? 0 : entries_.front().published;
    for (const auto& entryIt : channel.children("item"))
    {
        entry e;
        e.read = false;

        auto linkNode = entryIt.child("link");
        if (!linkNode)
            continue;
        e.url = linkNode.text().as_string();

        auto titleNode = entryIt.child("title");
        if (titleNode && titleNode.text())
            e.title = titleNode.text().as_string();
        auto dateNode = entryIt.child("pubDate");
        if (dateNode && dateNode.text())
        {
            std::istringstream in{dateNode.text().as_string()};
            date::sys_seconds tp;
            in >> date::parse("%a, %d %b %Y %T %z", tp);
            e.published = tp.time_since_epoch().count();
        }
        else
            continue;
        if (last < e.published || all)
            entries_.push_back(std::move(e));
    }
    std::sort(entries_.begin(), entries_.end(), [](const auto& a, const auto& b)
    {
        return a.published > b.published;
    });
}

void feed::update(bool all)
{
    class url u(url_);
    httplib::Client cli(u.scheme_host());
    cli.enable_server_certificate_verification(false);
    auto result = cli.Get(u.path_query());
    if (result)
    {
        pugi::xml_document doc;
        const pugi::xml_parse_result xmlResult = doc.load_string(result.value().body.c_str());
        if (xmlResult.status == pugi::status_ok)
        {
            auto root = doc.root();
            if (auto nd = root.child("feed"))
            {
                parse_feed(nd, all);
                return;
            }
            if (auto nd = root.child("rss"))
            {
                parse_rss(nd, all);
                return;
            }
        }
        else
            std::cerr << xmlResult.description() << std::endl;
    }
    else
        std::cerr << result.error() << std::endl;
}
