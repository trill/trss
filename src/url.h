/*
 * This file is part of the trss.
 * Copyright (c) 2021 Stefan Ascher
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <string>

class url
{
public:
    url(const std::string& aurl);
    const std::string& scheme() const { return scheme_; }
    const std::string& host() const { return host_; }
    const std::string& path() const { return path_; }
    const std::string& query() const { return query_; }
    std::string scheme_host() const;
    std::string path_query() const;
private:
    void parse(const std::string& aurl);
    std::string scheme_, host_, path_, query_;
};
