/*
 * This file is part of the trss.
 * Copyright (c) 2021 Stefan Ascher
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "utils.h"
#include <wordexp.h>
#include <spawn.h>
#include <unistd.h>
#include <iostream>
#include <cstring>

std::string expand_path(const char* path)
{
    wordexp_t p;
    wordexp(path, &p, 0);
    std::string result;
    if (p.we_wordc != 0)
    {
        result = p.we_wordv[0];
    }
    wordfree(&p);
    return result;
}

void run_command(const std::string& command)
{
    char* cmd = (char*)command.c_str();
    pid_t pid = 0;
    char* argv[] = { (char*)"sh", (char*)"-c", cmd, NULL };
    int status = posix_spawn(&pid, "/bin/sh", NULL, NULL, argv, environ);
    if (status != 0)
        std::cerr << "posix_spawn: " << strerror(status) << std::endl;
}
