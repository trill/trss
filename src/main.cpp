/*
 * This file is part of the trss.
 * Copyright (c) 2021-2022 Stefan Ascher
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <unistd.h>
#include <cstring>
#include <fstream>
#include <iostream>
#include <sstream>
#include <getopt.h>
#include <chrono>
#include <mutex>
#include <pugixml.hpp>
#include <thread>
#include <vector>
#include "feed.h"
#include "utils.h"

static bool urlOnly = false;
static bool openLast = false;
static bool unreadOnly = false;
static bool showLast = false;
static bool showAll = false;
static bool countOnly = false;
static bool markRead = false;

static void run(std::vector<feed>& feeds)
{
    if (countOnly || markRead)
    {
        size_t result = 0;
        for (auto& feed : feeds)
        {
            feed.update(showAll);
            for (const auto& entry : feed.entries_)
            {
                if (!entry.read)
                    ++result;
            }
        }
        if (markRead)
            std::cout << 0 << std::endl;
        else
            std::cout << result << std::endl;
        return;
    }

    for (auto& feed : feeds)
    {
        feed.update(showAll);

        if (!urlOnly && !openLast)
            std::cout << "\033[32m" << feed.title_ << "\033[39m" << std::endl;
        size_t i = 0;
        for (const auto& entry : feed.entries_)
        {
            if (openLast)
            {
                if (!entry.url.empty())
                {
                    if (!unreadOnly || !entry.read)
                    {
                        std::string command = (feed.open_width_.empty() ? "xdg-open" : feed.open_width_)
                            + (" \"" + entry.url + "\"");
                        run_command(command);
                        if (unreadOnly)
                            return;
                    }
                }
                break;
            }
            if (showLast)
            {
                if (!urlOnly)
                {
                    char buff[20];
                    strftime(buff, 20, "%Y-%m-%d %H:%M:%S", localtime(&entry.published));
                    std::cout << "  " << buff << " \033[1m" << entry.title << "\033[0m \033[94m" << entry.url
                              << "\033[39m";
                }
                else
                    std::cout << entry.url;
                std::cout << std::endl;
                continue;
            }
            if (entry.read)
                continue;
            ++i;
            if (!urlOnly && !openLast)
            {
                if (!urlOnly)
                {
                    char buff[20];
                    strftime(buff, 20, "%Y-%m-%d %H:%M:%S", localtime(&entry.published));
                    std::cout << "  " << buff << " \033[1m" << entry.title << "\033[0m \033[94m" << entry.url
                              << "\033[39m";
                }
                else
                    std::cout << entry.url;
                std::cout << std::endl;
            }
            else
                std::cout << entry.url << std::endl;
        }
        if (!urlOnly && !openLast)
            std::cout << i << " new" << std::endl;
    }
}

static void read_urls(std::vector<feed>& feeds)
{
    constexpr const char* URLS_FILE = "~/.config/trss/urls";
    const std::string fileName = expand_path(URLS_FILE);
    std::ifstream file(fileName);
    std::string str;
    while (std::getline(file, str))
    {
        if (!str.empty())
        {
            const auto parts = split(str, ",", false, false);
            feed f(parts[0]);
            if (parts.size() > 1)
                f.open_width_ = parts[1];
            feeds.emplace_back(std::move(f));
        }
    }
}

static void load_cache(std::vector<feed>& feeds)
{
    constexpr const char* CACHE_FILE = "~/.config/trss/cache";
    const std::string fileName = expand_path(CACHE_FILE);
    std::ifstream file(fileName);
    if (!file.good())
    {
        return;
    }
    auto find_feed = [&feeds](const std::string& url) -> feed*
    {
        for (auto& feed : feeds)
        {
            if (feed.url_ == url)
                return &feed;
        }
        return nullptr;
    };
    std::string str;
    while (std::getline(file, str))
    {
        if (str.empty())
            continue;
        auto parts = split(str, "\t", false, false);
        if (parts.size() < 2)
        {
            continue;
        }
        feed* f = find_feed(parts[0]);
        if (f)
        {
            entry e{ .url = "", .title = "", .published = atoll(parts[1].c_str()), .read = true };
            if (parts.size() > 2)
                e.url = parts[2];
            if (parts.size() > 3)
                e.title = parts[3];
            f->entries_.push_back(std::move(e));
        }
    }
}

static void save_cache(const std::vector<feed>& feeds)
{
    constexpr const char* CACHE_FILE = "~/.config/trss/cache";
    const std::string fileName = expand_path(CACHE_FILE);
    std::ofstream file(fileName);
    for (const auto& feed : feeds)
    {
        if (feed.entries_.empty())
            continue;

        file << feed.url_ << "\t" << feed.entries_.front().published << "\t" << feed.entries_.front().url << "\t"
             << feed.entries_.front().title << std::endl;
    }
}

int main(int argc, char** argv)
{
    bool noSave = false;
    bool listFeeds = false;
    int c = 0;
    static struct option longOptions[] = {
        { "open", no_argument, NULL, 'o' },
        { "open-unread", no_argument, NULL, 'O' },
        { "show-last", no_argument, NULL, 'l' },
        { "list-feeds", no_argument, NULL, 'L' },
        { "show-all", no_argument, NULL, 'a' },
        { "no-save", no_argument, NULL, 'n' },
        { "new-count", no_argument, NULL, 'c' },
        { "mark-read", no_argument, NULL, 'r' },
        { "url-only", no_argument, NULL, 'u' },
        { "help", no_argument, NULL, 'h' },
        { "version", no_argument, NULL, 'v' },
        { NULL, 0, NULL, 0 }
    };
    int optionIndex = 0;
    while ((c = getopt_long(argc, argv, "oOlLacnruhv", longOptions, &optionIndex)) != -1)
    {
        switch (c)
        {
        case 'n':
            noSave = true;
            break;
        case 'u':
            urlOnly = true;
            break;
        case 'o':
            openLast = true;
            break;
        case 'O':
            openLast = true;
            unreadOnly = true;
            break;
        case 'l':
            showLast = true;
            break;
        case 'L':
            listFeeds = true;
            break;
        case 'a':
            showAll = true;
            break;
        case 'c':
            countOnly = true;
            break;
        case 'r':
            markRead = true;
            break;
        case 'h':
            std::cout << "trss - Commandline RSS reader" << std::endl << std::endl;
            std::cout << "Usage:" << std::endl;
            std::cout << argv[0] << " [options] [<url> ...]" << std::endl;
            std::cout << std::endl;
            std::cout << "Options:" << std::endl;
            std::cout << "  -o --open              Open last post" << std::endl;
            std::cout << "  -O --open-unread       Open last unread post" << std::endl;
            std::cout << "  -l --show-last         Show last posts" << std::endl;
            std::cout << "  -L --list-feeds        Lists subscribed feeds" << std::endl;
            std::cout << "  -a --show-all          Show all posts" << std::endl;
            std::cout << "  -u --url-only          Just print URLs" << std::endl;
            std::cout << "  -n --no-save           Don't update cache" << std::endl;
            std::cout << "  -c --new-count         Show only number of new entries" << std::endl;
            std::cout << "  -r --mark-read         Mark all as read" << std::endl;
            std::cout << "  -h --help              Show help" << std::endl;
            std::cout << "  -v --version           Show program version" << std::endl;
            return 0;
        case 'v':
            std::cout << "trss 0.3" << std::endl;
            return 0;
        default:
            return 1;
        }
    }
    if (countOnly)
        noSave = true;
    if (markRead)
        noSave = false;

    std::vector<feed> feeds;
    if (optind < argc)
    {
        while (optind < argc)
            feeds.emplace_back(feed(argv[optind++]));
    }
    if (feeds.empty())
    {
        read_urls(feeds);
    }
    if (feeds.empty())
    {
        std::cerr << "No URLs" << std::endl;
        return 1;
    }
    load_cache(feeds);

    if (listFeeds)
    {
        for (const auto& f : feeds)
        {
            std::cout << f.url_;
            std::cout << std::endl;
        }
        return 0;
    }

    run(feeds);
    if (!noSave)
        save_cache(feeds);

    return 0;
}
