# trss

Commandline RSS reader. Extremely simple, it just prints new posts to the terminal.
Probably you could do the same in 10 lines of Python as well, or even Bash, but where would be the fun then?

## License

GPL 3.0

## Requirements

* [PugiXML](https://pugixml.org/) (on Arch you can do `sudo pacman -S pugixml`)
* C++20
* OpenSSL
* CMake
* `getopt.h`
* [cpp-httplib](https://github.com/yhirose/cpp-httplib) ([AUR](https://aur.archlinux.org/packages/cpp-httplib))
* [date](https://github.com/HowardHinnant/date) (included)

## Build

To build the program:

~~~sh
mkdir build
cd build
cmake ..
make
~~~

Optional:
~~~sh
sudo make install
~~~

## Usage

~~~sh
$ trss [options] [<url>...]
~~~

## Config

Make a `urls` file in `~/.config/trss` and add one URL per line.

Format:

~~~plain
<url>[,<open width>]
~~~

Example:

~~~plain
https://www.youtube.com/feeds/videos.xml?channel_id=UC3ts8coMP645hZw9JSD3pqQ,mpv
https://www.youtube.com/feeds/videos.xml?channel_id=UC-yuWVUplUJZvieEligKBkA,mpv
~~~

## Example

~~~plain
└─ $ ▶ rss
Andreas Kling
  2021-03-16 04:58:11 Browser hacking: Let's get the "Canvas Cycle" JS demo running in our Browser! https://www.youtube.com/watch?v=b3a5V45LLss
  2021-03-11 21:07:02 Cat talk: Letter from a 17-year old developer https://www.youtube.com/watch?v=yTv95AmoW5w
  2021-03-11 00:27:50 OS hacking: Write-protecting sensitive process variables (UID and more) https://www.youtube.com/watch?v=IvtM6bqB3uY
  2021-03-05 20:40:05 Let's port Diablo to SerenityOS! https://www.youtube.com/watch?v=ZOzZ8R4gphE
  2021-03-02 22:41:07 DevTools hacking: Whole system profiling (all processes) https://www.youtube.com/watch?v=AK9H6_pR0BM
  2021-02-28 23:40:30 Car talk: General update / reflections https://www.youtube.com/watch?v=mGNw1HJ1vOQ
  2021-02-28 20:37:08 SerenityOS update (February 2021) https://www.youtube.com/watch?v=M81Hy5UP2nA
  2021-02-27 22:04:25 DevTools hacking: Viewing individual profile samples (+ random profiler improvements) https://www.youtube.com/watch?v=wTXtg82dG8U
  2021-02-23 22:31:46 OS hacking: Let's profile program startup and make everything start faster! https://www.youtube.com/watch?v=IEiPL9PUVYw
  2021-02-22 21:52:55 Browser hacking: Let's do some work on CSS overflow! https://www.youtube.com/watch?v=39yyYYItte8
  2021-02-21 15:44:10 Let's choose some CLion giveaway winners! https://www.youtube.com/watch?v=C-NE3XQGBSs
  2021-02-19 20:28:33 OS hacking: Unmapping kernel functions after initialization https://www.youtube.com/watch?v=qOhh21A4814
  2021-02-18 19:53:19 OS hacking: Hardening ELF executables with RELRO https://www.youtube.com/watch?v=7kWSqqZCUcE
  2021-02-14 22:21:28 Car talk: Trying the CLion IDE + CLion giveaway! :^) https://www.youtube.com/watch?v=dac2GZPSt5E
  2021-02-14 19:02:01 OS hacking: Making some kernel memory read-only after booting finishes https://www.youtube.com/watch?v=X6s3XBWe2XU
15 new
javidx9
  2021-02-28 17:26:40 Super Fast Ray Casting in Tiled Worlds using DDA https://www.youtube.com/watch?v=NbSee-XM7WA
  2021-02-14 18:46:07 Networking in C++ Part #4: MMO - Designing Passivity https://www.youtube.com/watch?v=f_1lt9pfaEo
  2021-01-29 20:24:34 Circle Vs Rectangle Collisions (and TransformedView PGEX) https://www.youtube.com/watch?v=D2a5fHX-Qrs
  2020-12-13 08:15:43 Community Showcase 2020 https://www.youtube.com/watch?v=Qmr4BW0FK2s
  2020-11-21 20:19:06 Networking in C++ Part #3: MMO Client/Server Framework, Tweaks & Client Validation https://www.youtube.com/watch?v=hHowZ3bWsio
  2020-11-01 11:11:25 Controlling Elegoo Robot Smart Car with ASIO and C++ https://www.youtube.com/watch?v=nkCP95zLvSQ
  2020-10-24 09:59:36 A Little Update Video https://www.youtube.com/watch?v=0L2m2_Y0CWk
  2020-10-11 21:02:10 Networking in C++ Part #2: MMO Client/Server, ASIO, Sockets & Connections https://www.youtube.com/watch?v=UbjxGvrDrbw
  2020-10-04 00:03:51 Networking in C++ Part #1: MMO Client/Server, ASIO & Framework Basics https://www.youtube.com/watch?v=2hNdkYInj4g
  2020-09-20 18:39:23 Introducing RayCastWorld https://www.youtube.com/watch?v=Vij_obgv9h4
  2020-09-06 18:36:54 Coding Quickie: Integral Images https://www.youtube.com/watch?v=pXoDkHYmTxc
  2020-08-22 14:35:16 olcCodeJam2020 Announcement https://www.youtube.com/watch?v=DkupW31s2oA
  2020-08-15 21:56:49 Automating Sequences via Lua Coroutines in C++ https://www.youtube.com/watch?v=E42Lyv2Ra1c
  2020-07-26 18:12:13 [LIVE] Code-It-Yourself! Sliding Block Puzzle Games https://www.youtube.com/watch?v=l7YEaa2otVE
  2020-07-13 01:13:24 Arbitrary Rectangle Collision Detection & Resolution - Complete! https://www.youtube.com/watch?v=8JJ-4JgR7Dg
15 new
~~~
