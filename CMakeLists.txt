cmake_minimum_required (VERSION 3.16)
project (trss C CXX)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)
set(CMAKE_DEBUG_POSTFIX "")
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

include(CheckIncludeFile)
check_include_file("getopt.h" HAVE_GETOPT_H)
if (NOT HAVE_GETOPT_H)
    message(FATAL_ERROR "getopt.h not found")
endif()

find_package(PugiXML REQUIRED)
include(FindOpenSSL)
if (NOT ${OPENSSL_FOUND})
    message(FATAL_ERROR "OpenSSL not found")
endif()
include_directories(${PUGIXML_INCLUDE_DIR})
include_directories(${OPENSSL_INCLUDE_DIR})

add_compile_options(-Wall -Wextra -Werror -Wno-unknown-warning-option -fdiagnostics-color=always)

file(GLOB SOURCES src/*.cpp src/*.h)

add_executable(
    trss
    ${SOURCES}
)

target_link_libraries(trss pugixml::pugixml)
target_link_libraries(trss ${OPENSSL_LIBRARIES})

install(TARGETS trss DESTINATION bin)
